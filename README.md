Python GUI to track problematic pixel modules in the Atlas conditions DB

Currently, due to package dependencies on Qt5 and PyQT5, this only runs on lxplus

Setup

git clone https://gitlab.cern.ch/swahdan/PixelModuleTracker.git

cd PixelModuleTracker

cp ModuleTracker.json.template .ModuleTracker.json

Edit .ModuleTracker.json and modify the PASSWORD entry

"PASSWORD": change it to a valid password

source setup.sh

./dumpModDb.py filename.csv 

./dumpModDb.py -h for delimiter and quote character options

addModEntry.py demonstrates how to make a new entry from a script.

./ModuleTracker.py  to start to GUI with the test table to look at the newly created entry from the script.
