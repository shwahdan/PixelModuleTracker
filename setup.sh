if [ -x /det/tdaq/scripts/setup_TDAQ_tdaq-07-01-00.sh ] ;then
#point 1 setup
source /det/tdaq/scripts/setup_TDAQ_tdaq-07-01-00.sh
export QTDIR=$LCG_INST_PATH/LCG_87/qt5/5.6.0/$CMTCONFIG
export LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH
export QT_PLUGIN_PATH=$QTDIR/plugins
export PYTHONPATH=$LCG_INST_PATH/pyqt5/5.5.1-5f024/$CMTCONFIG/lib/python2.7/site-packages/:$PYTHONPATH
else
#lxplus setup
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
lsetup "lcgenv -p LCG_94python3 x86_64-slc6-gcc62-opt Python"
lsetup "lcgenv -p LCG_94python3 x86_64-slc6-gcc62-opt Qt5" #Qt5        
lsetup "lcgenv -p LCG_94python3 x86_64-slc6-gcc62-opt pyqt5" #PyQt
lsetup "lcgenv -p LCG_94python3 x86_64-slc6-gcc62-opt cx_oracle" #cx_Oracle 
#export QT_PLUGIN_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/qt5/5.6.0/x86_64-slc6-gcc62-opt/plugins
fi

export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb
export PIX_MOD_TRACKER_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ -r env/bin/activate ] 
then 
source env/bin/activate
else
python3 -m venv env
source  env/bin/activate
pip3 install dash dash-html-components dash-core-components dash-table dash_daq pandas
fi
