#!/usr/bin/env python

from PixDb import *
import os
import csv
import argparse


parser = argparse.ArgumentParser(description='Tool to dump the Module Tracker DB to CSV file')
parser.add_argument('-d','--delimiter',action='store_true',default= " ",
                    help="delimiter used in CSV, default=' '")
parser.add_argument('-q','--quotechar',action='store_true',default='"',
                    help="""quote char used in CSV, default='"'""")

parser.add_argument('outfile', metavar='outfile', type=str,nargs='?',
                   help='outfile for CSV')

args = vars(parser.parse_args())
if(args["outfile"] is None):
    print ('%s: error: no output file specified' % sys.argv[0])
    exit(-1)

db=PixDb(os.environ["PIX_MOD_TRACKER_PATH"]+"/.ModuleTracker.json")
res=db.query_all()

with open(args["outfile"], 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=args["delimiter"],
                            quotechar=args["quotechar"], quoting=csv.QUOTE_MINIMAL)
    for r in res:
        n=db.unpack_record(r)
        writer.writerow(n)
