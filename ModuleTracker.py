#!/usr/bin/env python

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PixDb import *
import os
# sort module IDs by 1number


class MainWindow(QWidget):
    def query(self):
        res=self.db.query_all()
        self.table.setSortingEnabled(False)
        self.table.clear()
        rows=len(res)
        cols=0
        if(rows>0): cols=len(res[0])+1
        self.table.setRowCount(rows)
        self.table.setColumnCount(cols)
        self.table.sortItems(0)
        headers="DB ID","Connectivity","Module ID","Time Entered","Time Observed","User","Actions","Priority","Symptoms","Current Hypothesis","Diagnosis","Comments"
        col_width=[60,130,70,150,150,80,120,120,120,120,120,120]
      
        y=0
    
        for x in range(cols):            
            self.table.setColumnWidth(x,col_width[x])
            self.table.setHorizontalHeaderItem(x,QTableWidgetItem(headers[x])) 
        
        for r in range(len(res)):
            for c in range (5):
                t=QTableWidgetItem(str(res[r][c+1]))
                t.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
                self.table.setItem(r,c+2,t)
            s0=self.db.mod_to_conn[res[r][1]]
            t0=QTableWidgetItem(s0)
            t0.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            self.table.setItem(r,1,t0)
            ss=str(res[r][0])
            tt=QTableWidgetItem(ss)
            tt.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            self.table.setItem(r,0,tt)
            s1=str(self.db.unpack_actions(res[r][5]))
            t1=QTableWidgetItem(s1)
            t1.setToolTip(s1)
            t1.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            self.table.setItem(r,6,t1)
            s2=str(self.db.priorities[ int(res[r][6]) ] )
            t2=QTableWidgetItem(s2)
            t2.setToolTip(s2)
            t2.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            self.table.setItem(r,7,t2)
            s3=str(self.db.unpack_symptoms( res[r][7]))
            t3=QTableWidgetItem(s3)
            t3.setToolTip(s3)
            t3.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            self.table.setItem(r,8,t3)
            s4=str(self.db.unpack_diagnosis( res[r][8]))
            t4=QTableWidgetItem(s4)
            t4.setToolTip(s4)
            t4.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            self.table.setItem(r,9,t4)
            s5=str(self.db.unpack_diagnosis( res[r][9]))
            t5=QTableWidgetItem(s5)
            t5.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            self.table.setItem(r,10,t5)
            s6=str(res[r][10])
            t6=QTableWidgetItem(s6.split('\n')[0]+ " ...")
            t6.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEnabled)
            t6.setToolTip(s6)
            self.table.setItem(r,11,t6)  
        self.table.verticalHeader().hide()
        #self.table.setColumnHidden(0,True)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setSortingEnabled(True)
        self.table.clicked.connect(self.select)
        self.editButton.setEnabled(False)
        self.removeButton.setEnabled(False)

    def __init__(self):
        super(MainWindow,self).__init__()
        self.table=QTableWidget()
        self.table.setMinimumWidth(1200)
        self.table.setMinimumHeight(800)
        vbox=QVBoxLayout()
        vbox.addWidget(self.table)
        hbox=QHBoxLayout()
        self.newButton=QPushButton("New Module Entry")
        self.newButton.clicked.connect(self.new)
        self.newButton.setMaximumWidth(200)
        self.editButton=QPushButton("Edit Module Entry")
        self.editButton.clicked.connect(self.edit)
        self.editButton.setMaximumWidth(200)  
        self.editButton.setEnabled(False)
        self.cancelButton=QPushButton("Exit")
        self.cancelButton.clicked.connect(self.cancel)
        self.cancelButton.setMaximumWidth(200)
        self.removeButton=QPushButton("Remove from DB")
        self.removeButton.clicked.connect(self.remove)
        self.removeButton.setMaximumWidth(200)
        self.removeButton.setEnabled(False)
        hbox.addWidget(self.newButton);
        hbox.addWidget(self.editButton);
        hbox.addWidget(self.removeButton);
        hbox.addWidget(self.cancelButton);
        vbox.addLayout(hbox)
        
        self.db=PixDb(os.environ["PIX_MOD_TRACKER_PATH"]+"/.ModuleTracker.json")
    
        self.query()
      
     #   self.setGeometry(200, 200,1200, 900)
        self.setMinimumWidth(1400)
        self.setLayout(vbox)    
        self.show()

    def new(self):        
        d=PixEntry(self.db)
        d.setWindowTitle("New Module Entry")
        d.exec_()
        self.query()
    def edit(self):     
        row=self.table.selectionModel().selectedRows()[0].row()
        id=self.table.item(row,0).text()
        d=PixEntry(self.db,id)
        d.setWindowTitle("Edit Module Entry")
        d.exec_()
        self.query()

    def remove(self):
        row=self.table.selectionModel().selectedRows()[0].row()
        id=self.table.item(row,0).text()
        conn=self.table.item(row,1).text()
        mod=self.table.item(row,2).text()                
        res=QMessageBox.question(self,"Caution","Are your sure you want to remove this entry (ID="+id +" )for "+conn+" ("+mod +") from the DB")
        if(res==QMessageBox.Yes):
             self.db.remove(id)
        self.query()

    def select(self):
        self.removeButton.setEnabled(True)
        self.editButton.setEnabled(True)

    def cancel(self):
        sys.exit()
    

class ModuleListItem(QListWidgetItem):
    def __lt__(self, other):
        return int(self.text()[1:]) < int(other.text()[1:])

class myListWidget(QListWidget):
    def __init__(self,name):
        super(myListWidget,self).__init__()
        self.name=name

    def Clicked(self,item):        
        parent=self.parentWidget()
        sel=item.text()
        db=parent.db
        if(parent.id!=None and (self.name=="ROD" or self.name=="Module" or self.name=="Connectivity")): return
        if(self.name=="ROD"):
            parent.widgets["Module"].clear()
            parent.widgets["Connectivity"].clear()
            parent.widgets["Module"].addItems(db.rod_to_mod[sel])
            parent.widgets["Connectivity"].addItems(db.rod_to_conn[sel])
            parent.resetButton.setEnabled(True)
        elif(self.name=="Module"):
            it=parent.widgets["Connectivity"].findItems(db.mod_to_conn[sel],Qt.MatchExactly)
            parent.widgets["Connectivity"].setCurrentItem(it[0])
            it=parent.widgets["ROD"].findItems(db.mod_to_rod[sel],Qt.MatchExactly)
            parent.widgets["ROD"].setCurrentItem(it[0])
        elif(self.name=="Connectivity"):
           it=parent.widgets["Module"].findItems(db.conn_to_mod[sel],Qt.MatchExactly)
           parent.widgets["Module"].setCurrentItem(it[0])
           it=parent.widgets["ROD"].findItems(db.conn_to_rod[sel],Qt.MatchExactly)
           parent.widgets["ROD"].setCurrentItem(it[0])
        elif(self.name=="Diagnosis" or self.name=='Hypothesis'):
            name=self.name
            w=parent.widgets[name]
            s=self.currentIndex().data()
            if(sel!='Unknown'):
                self.item(0).setSelected(False)
                res=QMessageBox.question(self,name+ " Selection","Are you sure to add '"+ sel + "' to "+name)
                i=self.findItems(sel,Qt.MatchExactly)[0]
                if(res==QMessageBox.Yes):
                    i.setSelected(True)
                else:
                    i.setSelected(False)
                    
            else:
                res=QMessageBox.question(self,"Diagnosis Selection","Are you sure to reset the "+name+" selection to Unknown")
                if(res==QMessageBox.Yes):
                    for c in range(1,self.count()):
                        self.item(c).setSelected(False)                
                    self.item(0).setSelected(True)
                else:
                    self.item(0).setSelected(False)
            


class PixEntry(QDialog):
    def __init__(self,db,id=None,read_only=True):
        super(PixEntry,self).__init__()
        self.db=db
        self.id=id
        self.widgets=OrderedDict()
        labels=("ROD","Module","Connectivity","Symptoms","Hypothesis","Diagnosis","Actions","Priority")
        hbox = QHBoxLayout()
        hbox1 = QHBoxLayout()
                
        vbox=OrderedDict()
        for l in labels:
            vbox[l]=QVBoxLayout()
            self.widgets[l]=myListWidget(l)

        self.widgets["Symptoms"].setSelectionMode(QAbstractItemView.MultiSelection)
        self.widgets["Diagnosis"].setSelectionMode(QAbstractItemView.MultiSelection)
        self.widgets["Hypothesis"].setSelectionMode(QAbstractItemView.MultiSelection)
        self.widgets["Actions"].setSelectionMode(QAbstractItemView.MultiSelection)
        self.widgets["Symptoms"].addItems(self.db.symptoms)
        self.widgets["Diagnosis"].addItems(self.db.diagnosis)
        self.widgets["Hypothesis"].addItems(self.db.diagnosis)
        self.widgets["Actions"].addItems(self.db.actions)
        self.widgets["Priority"].addItems(self.db.priorities)
        for r in self.db.rod_to_mod: self.widgets["ROD"].addItem(r)
        for m in self.db.mod_to_conn:
            self.widgets["Module"].addItem(ModuleListItem(m))
            self.widgets["Connectivity"].addItem(self.db.mod_to_conn[m])

        self.widgets["Connectivity"].itemClicked.connect(self.widgets["Connectivity"].Clicked)
        self.widgets["Module"].itemClicked.connect(self.widgets["Module"].Clicked)
        self.widgets["ROD"].itemClicked.connect(self.widgets["ROD"].Clicked)
        self.widgets["Diagnosis"].itemClicked.connect(self.widgets["Diagnosis"].Clicked)
        self.widgets["Hypothesis"].itemClicked.connect(self.widgets["Hypothesis"].Clicked)
        self.widgets["Connectivity"].setSortingEnabled(True)
        self.widgets["Module"].setSortingEnabled(True)
        self.widgets["ROD"].setSortingEnabled(True)
        self.widgets["Module"].sortItems()
        self.widgets["ROD"].sortItems()
        self.widgets["Connectivity"].sortItems()
        self.widgets["Module"].sortItems()
        self.widgets["ROD"].sortItems()
        self.widgets["Connectivity"].sortItems()
        self.widgets["Module"].setMinimumSize(80,200)
        self.widgets["ROD"].setMinimumSize(100,200)
        self.widgets["Connectivity"].setMinimumSize(140,200)
        self.widgets["Symptoms"].setMinimumSize(220,200)
        self.widgets["Diagnosis"].setMinimumSize(180,600)
        self.widgets["Hypothesis"].setMinimumSize(180,600)

        self.widgets["Actions"].setMinimumSize(120,200)
        self.widgets["Priority"].setMinimumSize(80,200)
        dt=QDateTimeEdit(QDateTime.currentDateTime())
        dt.setDisplayFormat("dd.MM.yyyy hh:mm:ss")
        dt.setCalendarPopup(True)
        dt.setWindowTitle("Date of Change")
        self.widgets["Observed"]=dt
        
        for k in vbox:
            vbox[k].addWidget(QLabel(k))            
            vbox[k].addWidget(self.widgets[k])
            vbox[k].setAlignment(Qt.AlignTop)
            hbox.addLayout(vbox[k])
        self.resetButton=QPushButton("Show All")
        self.resetButton.setEnabled(False)
        self.resetButton.clicked.connect(self.resetSel)
        self.searchEdit=QLineEdit()
        self.searchEdit.setMaxLength(30)
        self.searchEdit.setMaximumWidth(200)
        self.resetButton.setMaximumWidth(100)
        completer=QCompleter()
        self.searchEdit.setCompleter(completer)
        self.comment=QTextEdit()
        model=QStringListModel()
        completer.setModel(model)
        completer.setCompletionMode(QCompleter.PopupCompletion);
        model.setStringList(self.db.mod_list+self.db.conn_list)
        self.searchEdit.editingFinished.connect(self.lineSel)
        hbox1.addWidget(self.searchEdit)
        hbox1.addWidget(self.resetButton)
        hbox1.setAlignment(Qt.AlignLeft)
        vbox3=QVBoxLayout()
        vbox3.addWidget(QLabel("Time Observed"))        
        vbox3.addWidget(dt)
        vbox1=QVBoxLayout()
        vbox1.addLayout(hbox1)
        vbox1.addLayout(vbox3)
        vbox1.addLayout(hbox)
        vbox2=QVBoxLayout()
        vbox2.addWidget(QLabel("Comment"))        
        vbox2.addWidget(self.comment)
        hbox2=QHBoxLayout()
        self.submitButton=QPushButton("Submit to DB")
        self.submitButton.clicked.connect(self.submit)
        self.cancelButton=QPushButton("Close")
        self.cancelButton.clicked.connect(self.cancel)
        hbox2.addWidget(self.submitButton);
        hbox2.addWidget(self.cancelButton);
        
        dt.setMaximumWidth(200)

        vbox1.addLayout(vbox3)
        vbox1.addLayout(vbox2)
        vbox1.addLayout(hbox2)
        
        #hbox.addWidget(self.comment)
        self.setGeometry(300, 300, 300, 150)
        
        self.setLayout(vbox1) 
        if(self.id!=None):
            self.setFromDB()
        else:
            self.resetSel("all")
        self.show()
    def setFromDB(self):
        res=self.db.query_id(self.id)
        mid=res[0]
        w=self.widgets["Module"]
        it=w.findItems(mid,Qt.MatchExactly)
        w.setCurrentItem(it[0])
#        w.setSelectionMode(QAbstractItemView.NoSelection)
        w.setEnabled(False)
        w=self.widgets["Connectivity"]
        it=w.findItems(self.db.mod_to_conn[mid],Qt.MatchExactly)
        w.setCurrentItem(it[0])   
#        w.setSelectionMode(QAbstractItemView.NoSelection)
        w.setEnabled(False)
        w=self.widgets["ROD"]
        it=w.findItems(self.db.mod_to_rod[mid],Qt.MatchExactly)
        w.setCurrentItem(it[0])
#        w.setSelectionMode(QAbstractItemView.NoSelection)
        w.setEnabled(False)
        w=self.widgets["Priority"]
        w.item(int(res[5])).setSelected(True);
        tables={ "Actions":4,"Symptoms":6,"Hypothesis":7,"Diagnosis":8  }
        for key in tables:
            w=self.widgets[key]
            w.reset()
            i=0
            for c in res[tables[key]]:
                if(c=='1'): w.item(i).setSelected(True)
                i+=1;
        self.widgets["Observed"].setDateTime(res[2])
        self.comment.setPlainText(res[9])

    def cancel(self):
        self.close()

    def submit(self):        
        mod=self.widgets["Module"].selectedItems()
        symptom=self.widgets["Symptoms"].selectedItems()
        diag=self.widgets["Diagnosis"].selectedItems()
        hyp=self.widgets["Hypothesis"].selectedItems()
        action=self.widgets["Actions"].selectedItems()
        prio=self.widgets["Priority"].selectedItems()
        error=""
        if(len(mod)!=1):
            error="No Module selected"
        elif(len(symptom)==0):
            error="At least one symptom needs to be selected"
        elif(len(diag)==0):
            error="At least one diagnosis needs to be selected"
        elif(len(hyp)==0):
            error="At least one hypothesis needs to be selected"
        elif(len(action)==0):
             error="At least one action needs to be selected"
        elif(len(prio)==0):
             error="No priority selected"
        else:
            mod_sel=mod[0].text()
            sym_sel=self.db.pack_symptoms(symptom)
            diag_sel=self.db.pack_diagnosis(diag)
            hyp_sel=self.db.pack_diagnosis(hyp)
            action_sel=self.db.pack_actions(action)
            prio_sel=self.db.priorities.index(prio[0].text())
            ts_sel=self.widgets["Observed"].dateTime().toString("yyyy-MM-dd hh:mm:ss")
            username=os.environ.get("USER")
            comment_sel=self.comment.toPlainText()

            record={
                     "timestamp": ts_sel,
                     "username": username,
                     "actions":  action_sel,
                     "priority": prio_sel,
                     "symptoms": sym_sel,
                     "hypothesis":  hyp_sel,
                     "diagnosis": diag_sel,
                     "comment": comment_sel
                     }
            if(self.id==None):
                self.db.insert(mod_sel,record)
            else:
                self.db.update(self.id,record)                                         
            if(self.id==None):
                QMessageBox.information(self,"Success","Succesfully added new DB entry for "+ mod_sel)
                self.resetSel("all")
            else:
                QMessageBox.information(self,"Success","Succesfully edited DB entry (id="+str(self.id) + ") for "+ mod_sel) 
                self.close()
            
        if(error!=""):
            QMessageBox.critical(self,"Error",error)
        
    def resetSel(self,what=""):
        if(self.id==None):
            self.widgets["Module"].clear()
            self.widgets["Connectivity"].clear()            
            for m in self.db.mod_to_conn:
                self.widgets["Module"].addItem(ModuleListItem(m))
            self.widgets["Connectivity"].addItem(self.db.mod_to_conn[m])
            self.resetButton.setEnabled(False)
            self.widgets["ROD"].reset()
        if(what=="all"):
            self.widgets["Observed"].setDateTime(QDateTime.currentDateTime())
            it= self.widgets["Diagnosis"].findItems("Unknown",Qt.MatchExactly)
            self.widgets["Diagnosis"].setCurrentItem(it[0])
            it= self.widgets["Hypothesis"].findItems("Unknown",Qt.MatchExactly)
            self.widgets["Hypothesis"].setCurrentItem(it[0])
                                              
            self.widgets["Actions"].reset()
            self.comment.clear()
            self.widgets["Symptoms"].reset()
            self.widgets["Priority"].reset()
            
    def lineSel(self):
        if(self.id!=None): return
        sel=self.searchEdit.text()
        self.widgets["Module"].clear()
        self.widgets["Connectivity"].clear()
        self.resetButton.setEnabled(False)
        for m in self.db.mod_to_conn:
            self.widgets["Module"].addItem(ModuleListItem(m))
            self.widgets["Connectivity"].addItem(self.db.mod_to_conn[m])
        if(sel in self.db.mod_list):
            it=self.widgets["Connectivity"].findItems(self.db.mod_to_conn[sel],Qt.MatchExactly)
            self.widgets["Connectivity"].setCurrentItem(it[0])
            it=self.widgets["Module"].findItems(sel,Qt.MatchExactly)
            self.widgets["Module"].setCurrentItem(it[0])
            it=self.widgets["ROD"].findItems(self.db.mod_to_rod[sel],Qt.MatchExactly)
            self.widgets["ROD"].setCurrentItem(it[0])
        elif(sel in self.db.conn_list):
            it=self.widgets["Module"].findItems(self.db.conn_to_mod[sel],Qt.MatchExactly)
            self.widgets["Module"].setCurrentItem(it[0])
            it=self.widgets["ROD"].findItems(self.db.conn_to_rod[sel],Qt.MatchExactly)
            self.widgets["ROD"].setCurrentItem(it[0])
            it=self.widgets["Connectivity"].findItems(sel,Qt.MatchExactly)
            self.widgets["Connectivity"].setCurrentItem(it[0])
        else:
            self.searchEdit.clear()
        	
if __name__ == '__main__':
   app= QApplication(sys.argv)
   mainWindow=MainWindow()
   sys.exit(app.exec_())
