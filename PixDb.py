from collections import OrderedDict
import cx_Oracle
import sys
import os
import json
class PixDb():
    col_names= [ 
        "ID",
        "MID",
        "Connection",
        "Entered",
        "Observed",
        "User",
        "Actions",
        "Pri",
        "Symptoms",
        "Hypothesis",
        "Diagnosis",
        "Comment"
        ]
    def __init__(self,config):
        data = json.load(open(os.path.expanduser(config)))['int8r']        
        self.int8r="( \
        DESCRIPTION= \
        (ADDRESS= (PROTOCOL=TCP) (HOST=%s) (PORT=%d) ) \
        (LOAD_BALANCE=on) \
        (ENABLE=BROKEN) \
        (CONNECT_DATA= \
		(SERVER=DEDICATED) \
                (SERVICE_NAME=%s) \
        ) \
)" % (data["HOST"],data["PORT"], data["SERVICE_NAME"])
        self.connection=data["DATABASE"]        
        self.password=data["PASSWORD"]
        try:
            self.db_table=data["TABLE"]
        except:
            self.db_table="TEST_HISTORY"            
        rod_to_mod=OrderedDict()
        rod_to_conn=OrderedDict()
        mod_to_rod=OrderedDict()
        mod_to_conn=OrderedDict()
        conn_to_mod=OrderedDict()
        conn_to_rod=OrderedDict()        
        conn_list=list()
        mod_list=list()
        diagnosis=list()
        actions=list()
        symptoms=list()
        priorities=list() 
        with open('modlist.txt') as f:
            content = f.readlines()
            for l in content[1:]:
                l=l.split()
                rod=l[0]
                mod=l[1]
                conn=l[2]
                if rod not in rod_to_mod:                
                    rod_to_mod[rod]=list()
                    rod_to_conn[rod]=list()
                rod_to_mod[rod].append(mod)
                rod_to_conn[rod].append(conn)
                conn_to_mod[conn]=mod
                mod_to_conn[mod]=conn
                mod_to_rod[mod]=rod
                conn_to_rod[conn]=rod
                conn_list.append(conn)
                mod_list.append(mod)
        db=cx_Oracle.connect(self.connection,self.password,self.int8r)
        cursor = db.cursor()
        cursor.execute("select * from history_symptoms")
        for x in cursor.fetchall(): symptoms.append(x[1])
        cursor.execute("select * from history_diagnosis")
        for x in cursor.fetchall(): diagnosis.append(x[1])
        cursor.execute("select * from history_actions")
        for x in cursor.fetchall(): actions.append(x[1])
        cursor.execute("select * from history_priorities")
        for x in cursor.fetchall(): priorities.append(x[1])
        db.close()
        self.rod_to_mod=rod_to_mod
        self.rod_to_conn=rod_to_conn
        self.mod_to_rod=mod_to_rod
        self.mod_to_conn=mod_to_conn
        self.conn_to_mod=conn_to_mod
        self.conn_to_rod=conn_to_rod
        self.conn_list=conn_list
        self.mod_list=mod_list
        self.diagnosis=diagnosis        
        self.actions=actions
        self.symptoms=symptoms
        self.priorities=priorities
    def connect(self):
        return cx_Oracle.connect(self.connection,self.password,self.int8r)

    def query_id(self,id):
        db=self.connect()
        cursor = db.cursor()
        cursor.execute("""select
                    "MODULESERIAL","SYSTEMDATE","OBSERVED","USERNAME","ACTIONS","PRIORITY","SYMPTOMS","HYPOTHESIS","DIAGNOSIS","COMMENT" from """+self.db_table+ " where id="+str(id))
        res=cursor.fetchall()[0]
        db.close()
        return res

    def query_all(self):
        db=self.connect()
        cursor = db.cursor()
        cursor.execute("""select
                    "ID","MODULESERIAL","SYSTEMDATE","OBSERVED","USERNAME","ACTIONS","PRIORITY","SYMPTOMS","HYPOTHESIS","DIAGNOSIS","COMMENT" from """+self.db_table)

        res=cursor.fetchall()
        db.close()
        return res
    

    def insert(self,mid,record):
        db=self.connect()
        cursor = db.cursor()
        cursor.execute("insert into "+self.db_table+                                                                                                                       
                    """("MODULESERIAL","OBSERVED","USERNAME","ACTIONS","PRIORITY","SYMPTOMS","HYPOTHESIS","DIAGNOSIS","COMMENT")                                        
                    VALUES(:mod_sel,TO_DATE(:ts_sel,'yyyy-mm-dd hh24:mi:ss'),:username,:actions_sel,:prio_sel,:sym_sel,:hyp_sel,:diag_sel,:comment_sel)""" ,
                       mod_sel=mid,
                       ts_sel=record["timestamp"],
                       username=record["username"],
                       actions_sel=record["actions"],
                       prio_sel=record["priority"],
                       sym_sel=record["symptoms"],
                       hyp_sel=record["hypothesis"],
                       diag_sel=record["diagnosis"],
                       comment_sel=record["comment"]
                       )
        db.commit()
        db.close()

    def update(self,mid,record):
        db=self.connect()
        cursor=db.cursor()
        cursor.execute("update "+self.db_table+" set "+
            """ "OBSERVED"=TO_DATE(:ts_sel,'yyyy-mm-dd hh24:mi:ss'),                                                                   
                                                     "USERNAME"=:username,
                                                     "ACTIONS"=:actions_sel,
                                                     "PRIORITY"=:prio_sel,
                                                     "SYMPTOMS"=:sym_sel,
                                                     "HYPOTHESIS"=:hyp_sel,
                                                     "DIAGNOSIS"=:diag_sel,
                                                     "COMMENT"=:comment_sel where ID=:rid""",
                       ts_sel=record["timestamp"],
                       username=record["username"],
                       actions_sel=record["actions"],
                       prio_sel=record["priority"],
                       sym_sel=record["symptoms"],
                       hyp_sel=record["hypothesis"],
                       diag_sel=record["diagnosis"],
                       comment_sel=record["comment"],
                       rid=mid
                       )
        db.commit()
        db.close()

    def remove(self,mid):
        db=self.connect()
        cursor = db.cursor()
        cursor.execute("DELETE from "+self.db_table+" WHERE ID="+mid)
        db.commit()
        db.close()

    def check(self,what,s):
        ret=True
        attr=getattr(self,what)
        ret=ret and (len(attr)==len(s))
        for c in s:
            ret=ret and (c=="1" or c=="0")
        return ret
    def check_record(self,mid,r):
        ret=True
        ret=ret and self.check("diagnosis",r["diagnosis"])
        ret=ret and self.check("symptoms",r["symptoms"])
        ret=ret and self.check("diagnosis",r["hypothesis"])
        ret=ret and self.check("actions",r["actions"])
        ret=ret and (r["priority"]>=0 and r["priority"]<len(self.priorities))
        ret=ret and (len(r['username'])>0)
        ret=ret and (len(r['comment'])>=0)
        ret=ret and (mid in self.mod_list)
        return ret

    def add_symptom(self,s,r=""):
        if(r==''): res=list("0"*len(self.symptoms))
        else: res=list(r)
        idx=self.symptoms.index(s)
        res[idx]="1"
        return "".join(res)

    def add_diagnosis(self,s,r=""):
        if(r==''): res=list("0"*len(self.diagnosis))
        else: res=list(r)
        idx=self.diagnosis.index(s)
        res[idx]="1"
        return "".join(res)

    def add_action(self,s,r=""):
        if(r==''): res=list("0"*len(self.actions))
        else: res=list(r)
        idx=self.actions.index(s)
        res[idx]="1"
        return "".join(res)
    def get_priority(self,s):        
        return self.priorities.index(s)
    
 
        
    def pack_symptoms(self,symptom):
        sym=list("0"*len(self.symptoms))
        for s in symptom: sym[ (self.symptoms.index(s.text()))]="1"
        sym_sel="".join(sym)
        return sym_sel        
    def pack_diagnosis(self,diag):    
        di=list("0"*len(self.diagnosis))
        for d in diag: di[ (self.diagnosis.index(d.text()))]="1"
        diag_sel="".join(di)
        return diag_sel

    def pack_actions(self,act):    
        ac=list("0"*len(self.actions))
        for a in act: ac[ (self.actions.index(a.text()))]="1"
        act_sel="".join(ac)
        return act_sel
    

    def unpack_diagnosis(self,diag):
        result=""
        for d in range(len(diag)):
            if diag[d]=="1":
                result+=self.diagnosis[d]+","
        return result[:-1]

    def unpack_symptoms(self,symptoms):
        result=""
        for s in range(len(symptoms)):
            if symptoms[s]=="1":
                result+=self.symptoms[s]+","
        return result[:-1] 
        
    def unpack_actions(self,act):
        result=""
        for a in range(len(act)):
            if act[a]=="1":
                result+=self.actions[a]+","
        return result[:-1] 
    
    def query_all_todict(self,toStrings=True):
        res=self.query_all()
        dat=OrderedDict()
        for row in res:            
            d=dict()
            if(toStrings):
                r=self.unpack_record(row)
            else:
                r=self.unpack_bitstrings(row)
            mid=r[1]
            observed=r[3]
            r.insert(2,self.mod_to_conn[r[1]])
 
            for i in range(len(r)):
                if i!=1: d[self.col_names[i]]=r[i]
            if mid not in dat:
                d["Entries"]=0
                dat[mid]=d                
            else:
                entries=dat[mid]["Entries"]+1
                if dat[mid]["Observed"]<observed: dat[mid]=d
                dat[mid]["Entries"]=entries
        return dat
    def bitstring_to_number(self,str):
        res=0
        for i in range(len(str)):
            if str[i]=="1":
                res=res | (1<<i)
        return res

    def unpack_bitstrings(self,r):
        n=list(r);
        n[5]=self.bitstring_to_number(r[5])
        n[6]=int(r[6])
        n[7]=self.bitstring_to_number(r[7])
        n[8]=self.bitstring_to_number(r[8])
        n[9]=self.bitstring_to_number(r[9])
        return n


    def unpack_record(self,r):
        n=list(r);
        n[5]=self.unpack_actions(r[5])
        n[6]=self.priorities[int(r[6])]
        n[7]=self.unpack_symptoms(r[7])
        n[8]=self.unpack_diagnosis(r[8])
        n[9]=self.unpack_diagnosis(r[9])
        return n
