#!/usr/bin/env python

from PixDb import *
import os
import csv
import argparse
from time import gmtime, strftime

db=PixDb(os.environ["PIX_MOD_TRACKER_PATH"]+"/.ModuleTracker.json")
  
def get_record(sym):
    username = "Shayma"
    observed=strftime("%Y-%m-%d %H:%M:%S", gmtime())
    symptoms=db.add_symptom(sym)
    actions=db.add_action('Needs Work')
    diagnosis=db.add_diagnosis('Unknown')
    hypothesis=db.add_diagnosis('Unknown')
    priority=db.get_priority("Urgent")
    record= {
      "timestamp": observed,
      "actions":actions,
      "symptoms": symptoms,
      "username": username,
      "priority": priority,
      "diagnosis": diagnosis,
      "hypothesis" : hypothesis,
      "comment" : "from defects finder"
    }
    return record

#for i in db.query_all():
#    db.remove(str(i[0])) 

with open('/afs/cern.ch/user/s/swahdan/apixdqsoft/bad_module_finder/disabled_modules.csv', 'rb') as csvfile:
    next(csvfile)
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for i in reader:
        for x in i:
            arr = x.split(",")
            print arr[0], arr[1], arr[2]
            if arr[2] == 'modules_with_InactiveStatus':
                arr[2] = 'Data corruption' #to be changed to another symptom
            elif arr[2] == 'modules_with_SyncErrors':
                arr[2] = 'Desynchronized'
            elif arr[2] == 'modules_with_LowOccupancy':
                arr[2] = 'Low occupancy'
            var = raw_input("Do you want to enter the module to the DB: ")
            if var == "yes" or var == "YES" or var == "Y" or var == "y":
                db.insert(arr[0],get_record(arr[2]))
                print 'the module inserted to the DB'
            else:
                print 'the module will not be inserted to the DB'
                raise SystemExit

#if valid:
    #db.insert(mid,record)
#    print(db.query_all())

